<?php

class user {
    private $db;

    public function __construct($con) {
        $this->db = $con;
    }

    public function login($login, $pass) {

        $sql = $this->db->prepare("SELECT id,password FROM users WHERE name=:name");
        $sql->execute(array(':name'=> $login));

        $result = $sql->fetch(PDO::FETCH_ASSOC);

        if ($result){
            if (password_verify($pass,$result['password'])) {
                $_SESSION['logged'] = TRUE;
                $_SESSION['id'] = $result['id'];
                header('Location: adminPanel.php');
            }
            else {
                header('Location: index.php');
            }
        }
        else{
            header('Location: index.php');
        }
        die();
    }
    public function logout(){
        $_SESSION['logged'] = FALSE;
        header('Location: index.php');
        session_unset();
        die();
    }
}