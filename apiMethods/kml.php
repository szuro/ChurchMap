<?php

class kml {
    private $db;
    private $map;
    private $errorMessage;

    public function __construct($con, $map) {
        $this->db = $con;
        $this->map = $map;
    }


    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    function validate($file)
    {

        $xml=new DOMDocument();
        if(!$xml->load($file)) {
            throw new Exception('Nie udalo sie zaladowac pliku');
        }



        $placemarkExist = false;

        foreach ($xml->getElementsByTagName('Placemark') as $placemark) {

            $placemarkExist = true;

            $nameExist = false;
            $nameIsEmpty = true;

            $descriptionExist = false;
            $descriptionIsEmpty = true;

            $pointExist = false;

            $coordinatesExist = false;
            $coordinatesLongitudeIsNumber = false;
            $coordinatesLatitudeIsNumber = false;
            $coordinatesAltitudeIsNumber = false;

            foreach ($placemark->getElementsByTagName('name') as $name) {
                $nameExist = true;

                if($name->nodeValue != ""){
                    $nameIsEmpty = false;
                }
            }

            foreach ($placemark->getElementsByTagName('description') as $desc) {
                $descriptionExist = true;

                if($desc->nodeValue != ""){
                    $descriptionIsEmpty = false;
                }
            }

            foreach ($placemark->getElementsByTagName('Point') as $point) {

                $pointExist = true;

                foreach ($point->getElementsByTagName('coordinates') as $coordinates) {

                    $coordinatesExist = true;

                    $coordinate = $coordinates->nodeValue;
                    $coordinate = str_replace(" ", "", $coordinate);

                    $tmp = explode(",", $coordinate);

                    $coordinatesLongitudeIsNumber = is_numeric($tmp[0]);
                    $coordinatesLatitudeIsNumber = is_numeric($tmp[1]);
                    $coordinatesAltitudeIsNumber = is_numeric($tmp[2]);
                }

            }
            $PlacemarkError = false;
            $PlacemarkErrorDescription = "ERROR : ";


            if(!$nameExist){
                $PlacemarkError = true;
                $PlacemarkErrorDescription .= "Brak znacznika <name>, ";
            }
            if($nameIsEmpty){
                $PlacemarkError = true;
                $PlacemarkErrorDescription .= "Nazwa w znaczniku <name> nie moze byc pusta, ";
            }
            if(!$descriptionExist){
                $PlacemarkError = true;
                $PlacemarkErrorDescription .= "Brak znacznika <description>, ";
            }
            if($descriptionIsEmpty){
                $PlacemarkError = true;
                $PlacemarkErrorDescription .= "Opis w znaczniku <description> nie moze byc pusty, ";
            }
            if(!$pointExist){
                $PlacemarkError = true;
                $PlacemarkErrorDescription .= "Brak znacznika <Point>, ";
            }else{
                if(!$coordinatesExist){
                    $PlacemarkError = true;
                    $PlacemarkErrorDescription .= "Brak znacznika <coordinates>, ";
                }else{
                    if(!$coordinatesLongitudeIsNumber){
                        $PlacemarkError = true;
                        $PlacemarkErrorDescription .= "Wartosc Longitude musi byc numeryczna, ";
                    }
                    if(!$coordinatesLatitudeIsNumber){
                        $PlacemarkError = true;
                        $PlacemarkErrorDescription .= "Wartosc Latitude musi byc numeryczna, ";
                    }
                    if(!$coordinatesAltitudeIsNumber){
                        $PlacemarkError = true;
                        $PlacemarkErrorDescription .= "Wartosc Altitude musi byc numeryczna, ";
                    }
                }
            }

            if($PlacemarkError){
                $this->errorMessage = $PlacemarkErrorDescription;

                return false;
            }
        }

        if(!$placemarkExist){
            $this->errorMessage = "Brak znaczników <Placemark> w zaladowanym pliku!";
            return false;
        }

        return true;
    }



    public function importKML($file) {

        $xml=new DOMDocument();
        if(!$xml->load($file)) {
            throw new Exception('Nie udalo sie zaladowac pliku');
        }

        foreach($xml->getElementsByTagName('Placemark') as $placemark) {
            $name = $placemark->getElementsByTagName('name');
            $name = $name->item(0)->nodeValue;

            $address = $placemark->getElementsByTagName('description');
            $address = $address->item(0)->nodeValue;

            $point = $placemark->getElementsByTagName('Point');
            $coordinates = $point->item(0)->nodeValue;

            $coords = str_replace(" ", "", $coordinates);
            $coords = explode(",", $coords);

            $lng = doubleval($coords[0]);
            $lat = doubleval($coords[1]);
            $alt = doubleval($coords[2]);
            
            $this->map->addChurch($lat, $lng, $address, $name);
        }
    }

    public function exportkml()
    {
        $fileName = 'KMLfile.txt';
        $fileHandle = fopen('plikImport/KMLfile.txt', 'w');
        $result = $this->map->getAllChurches();
        
        fwrite($fileHandle, '<?xml version="1.0" encoding="UTF-8"?>'. PHP_EOL);
        fwrite($fileHandle, '<kml xmlns="http://www.opengis.net/kml/2.2">'. PHP_EOL);
        fwrite($fileHandle, '<Document>'. PHP_EOL);

        foreach($result['result'] as $item)
        {
            $id = $item['id'];
            $name = $item['name'];
            $description = $item['description'];
            $longitude = $item['longitude'];
            $latitude = $item['latitude'];
            $altitude = $item['altitude'];

            fwrite($fileHandle, '<Placemark>'. PHP_EOL);
            fwrite($fileHandle, '<name>'.$name.'</name>'. PHP_EOL);
            fwrite($fileHandle, '<description>'.$description.'</description>'. PHP_EOL);
            fwrite($fileHandle, '<Point>'. PHP_EOL);
            fwrite($fileHandle, '<coordinates>'.$longitude.','.$latitude.','.$altitude.'</coordinates>'. PHP_EOL);
            fwrite($fileHandle, '</Point>'. PHP_EOL);
            fwrite($fileHandle, '</Placemark>'. PHP_EOL);
        }

        fwrite($fileHandle, '</Document>'. PHP_EOL);
        fwrite($fileHandle, '</kml>'. PHP_EOL);

        header("Content-Type: application/vnd.google-earth.kml+xml");
        header("Content-Disposition: attachment; filename='churchmap.kml'");
        readfile('plikImport/KMLfile.txt');
        fclose($fileHandle);   

    }

}