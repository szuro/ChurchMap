<?php

class map {
    private $db;

    public function __construct($con) {
        $this->db = $con;
    }

    private function validateCoords($lat, $lng) {
        if(is_numeric($lat) and is_numeric($lng)) {
            if(abs($lng) <= 180 and abs(lat) <= 90 ) {
                return TRUE;
            }
        }
        return FALSE;
    }

    private function convertCoordsToFloat($records) {
        foreach($records as &$r) {
            foreach($r as $k=>$v){
                if(is_numeric($v)) { $r[$k] = floatval($v);
                }
            }
        }
        return $records;

    }

    public function addChurch($lat, $lng, $url, $name) {

        if(!$this->validateCoords($lat, $lng)) {
            return FALSE;
        }

        $sql = $this->db->prepare("INSERT INTO churches
            (name, description, longitude, latitude, altitude) VALUES
            (:name, :url, :lng, :lat, 0)");
        $sql->execute(array(':lat' => $lat,
                            ':lng' => $lng,
                            ':url' => $url,
                            ':name'=> $name
                            ));
    }

    public function getChurch($north, $south, $west, $east) {

        $sql = $this->db->prepare("SELECT * FROM churches WHERE
                longitude > :west AND longitude < :east AND
                latitude < :north AND latitude > :south");

        $sql->execute(array(':west' => $west,
                            ':east' => $east,
                            ':north' => $north,
                            ':south' => $south
                            ));

        $result = $sql->fetchAll(PDO::FETCH_ASSOC);

        return array('result' => $this->convertCoordsToFloat($result));
    }

    public function getAllChurches() {
        return $this->getChurch( 90, -90, -180, 180);
    }

    public function getChurchById($id) {
         $sql = $this->db->prepare("SELECT * FROM churches WHERE id = :id");

        $sql->execute(array(':id' => $id));

        $result = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $this->convertCoordsToFloat($result);
    }

    public function editChurch($id, $lat, $lng, $url, $name) {

        if($this->validateCoords($lat, $lng)) {

            $sql = $this->db->prepare("UPDATE churches SET
                        name = :name,
                        description = :url,
                        latitude = :lat,
                        longitude = :lng
                        WHERE id = :id");

            $sql->execute(array(':lat' => $lat,
                                ':lng' => $lng,
                                ':url' => $url,
                                ':name' => $name,
                                ':id' => $id
                                ));
        }
        return $this->getChurchById($id);
    }

    public function deleteChurch($id) {

        if(!is_int((int)$id)) {
            return FALSE;
        }

        $sql = $this->db->prepare("DELETE FROM churches WHERE id = :id");
        $sql->execute(array(':id'=>$id));

        echo "$id";
    }

    public function findChurch($name) {
        $sql = $this->db->prepare("SELECT * FROM churches WHERE name LIKE :name");

        $sql->execute(array(':name' => "%$name%"));

        $result = $sql->fetchAll(PDO::FETCH_ASSOC);

        return array('result' => $this->convertCoordsToFloat($result));
    }

    public function findChurchByLocation($longitude, $latitude, $radius ){

        $sql = $this->db->prepare("SELECT * FROM churches WHERE ((longitude - CAST(:longitude1 AS DECIMAL(38,12))) * (longitude - CAST(:longitude2 AS DECIMAL(38,12))) + (latitude - CAST(:latitude1 AS DECIMAL(38,12))) * (latitude - CAST(:latitude2 AS DECIMAL(38,12)))) <= (CAST(:radius1 AS DECIMAL(38,12)) * CAST(:radius2 AS DECIMAL(38,12)))");

        $sql->execute(array(':longitude1' => $longitude,
            ':longitude2' => $longitude,
            ':latitude1' => $latitude,
            ':latitude2' => $latitude,
            ':radius1' => $radius,
            ':radius2' => $radius
        ));

        $result = $sql->fetchAll(PDO::FETCH_ASSOC);

        return array('result' => $this->convertCoordsToFloat($result));
    }



}