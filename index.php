<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8" />
		<title>Mapa kościołów</title>
		<link href="style.css" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Libre+Franklin" rel="stylesheet">
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.0.min.js"></script>
    </head>
    <body>
        <?php
            session_start();
            if(isset($_SESSION['logged']) && $_SESSION['logged'] == TRUE){
                header('Location: adminPanel.php');
                die();
            }
        ?>
        <div id="mapa"></div>
        <div id="formularz">
            <div class="kontenerDanych">
                <form action="api.php" method="POST">
                    <input type="text" name="login" placeholder="Login" required />
                    <input type="password" name="password" placeholder="Hasło" required />
                    <input type="hidden" name="method" value="user.login">

                    <button type="submit">ZATWIERDŹ DANE</button>
                </form>
            </div>
            <div class="kontenerDanych">
                <span>Długość : </span><span id="dlugoscByLocation">Nie zaznaczono</span><br>
                <span>Szerokość : </span><span id="szerokoscByLocation">Nie zaznaczono</span>

                    <input type="text" name="radius" id="radius" placeholder="Promień (km)" required />
                    <button onclick="showTableSearchByLocation()">SZUKAJ PO LOKALIZACJI</button>

            <form action = "api.php" method = "GET">
                <input type="hidden" name="method" value="kml.exportkml">
                <button type="submit">EKSPORT KML</button>
            </form>
            </div>
                <input type="text" id="name" name="churchName" placeholder="Szukaj" />


                <div id="records"></div>
        </div>
        <div id="tableContainerByLocation" hidden>
            <table class="table" id="churchtableByLocation"></table>
            <button type="button" onclick="hideTableByLocation()">X</button>
        </div>
        <script>
            function initialize() {
                map = new google.maps.Map(document.getElementById('mapa'), {
                        draggableCursor: 'crosshair',
                        zoom: 8,
                        center: {lat: 54.546, lng: 18.453},
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                google.maps.event.addListener(map, 'click', grab);
                google.maps.event.addListener(map, 'idle', function() {
                    var bounds = map.getBounds();
                });
            }
        </script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDq9RzVDdJY8w5zZo2f43kH804_MDSWBpA&callback=initialize">
        </script>
        <script src="mapFunctions.js"></script>
    </body>
</html>
