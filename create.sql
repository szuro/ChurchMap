CREATE TABLE churches (
  [id] int NOT NULL PRIMARY KEY IDENTITY,
  [name] varchar(max) NOT NULL,
  [description] varchar(max) NOT NULL,
  [longitude] float NOT NULL,
  [latitude] float NOT NULL,
  [altitude] float NOT NULL
) ;

CREATE TABLE users (
[id] int NOT NULL PRIMARY KEY IDENTITY,
[name] varchar(max) NOT NULL,
[password] varchar(max) NOT NULL,
[adminrole] BIT DEFAULT 0
);