<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8" />
		<title>Mapa kościołów</title>
		<link href="style.css" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Libre+Franklin" rel="stylesheet">
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.0.min.js"></script>
    </head>
    <?php
        session_start();
        if(!isset($_SESSION['logged'])){
            header('Location: index.php');
            die();
        }
    ?>
    <body>

        <div id="mapa"></div>
        <div id="formularz">
            <div class="kontenerDanych">
                <form action='api.php' method="POST">
                    <input type="text" name="longitude" id="dlugoscGeograficzna" placeholder="Podaj długość geograficzną" required />
                    <input type="text" name="latitude" id="szerokoscGeograficzna" placeholder="Podaj szerokość geograficzną" required />
                    <input type="text" name="name" id="nazwaKosciola" placeholder="Podaj nazwę parafii" required />
                    <input type="text" name="url" id="adresKosciola" placeholder="Podaj adres www parafii" required />
                    <input type="hidden" name="method" value="map.addchurch" />
                    <button type="submit">ZATWIERDŹ DANE</button>
                </form>
            </div>
            <div class="kontenerDanych">
                <div>
                <label class="fileContainer" id="etykieta">
                    Plik KML
                    <input type="file" name="plikKML" id="plikKML" placeholder="Podaj ścieżkę pliku KML" />
                </label>
                </div><span>
                <button type="button" onclick="uploadKML()" class="smallbutton">ZATWIERDŹ PLIK</button>
                <form action = "api.php" method = "GET">
                <input type="hidden" name="method" value="kml.exportkml">
                <button type="submit" class="smallbutton">EKSPORT KML</button>
            </form></span>
            </div>
            <div class="kontenerDanych">
                <button type="button" onclick="showTable()">TABELA KOŚCIOŁÓW</button>
                
            </div>
            <div class="kontenerDanych">
                <form action="api.php" method="POST">
                    <input type="hidden" name="method" value="user.logout">
                    <button type="submit">WYLOGUJ</button>
                </form>
            </div>
            <div class="kontenerDanych">
                <span hidden>Długość : </span><span id="dlugoscByLocation" hidden>Nie zaznaczono</span>
                <span hidden>Szerokość : </span><span id="szerokoscByLocation" hidden>Nie zaznaczono</span>

                <input type="text" name="radius" id="radius" placeholder="Promień (km)" required />
                <button onclick="showTableSearchByLocation()">SZUKAJ PO LOKALIZACJI</button>

            </div>
            <input type="text" id="name" name="churchName" placeholder="Szukaj" />
            <div id="records"></div>

        </div>
        <div id="tableContainer" hidden>
            <table class="table" id="churchtable"></table>
            <button type="button" onclick="hideTable()">X</button>
        </div>
        <div id="tableContainerByLocation" hidden>
            <table class="table" id="churchtableByLocation"></table>
            <button type="button" onclick="hideTableByLocation()">X</button>
        </div>
        <script>
            function initialize() {
                map = new google.maps.Map(document.getElementById('mapa'), {
                        draggableCursor: 'crosshair',
                        zoom: 8,
                        center: {lat: 54.546, lng: 18.453},
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                google.maps.event.addListener(map, 'click', grab);
                google.maps.event.addListener(map, 'idle', function() {
                    var bounds = map.getBounds();
                });
            }
        </script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDq9RzVDdJY8w5zZo2f43kH804_MDSWBpA&callback=initialize">
        </script>
        <script src="mapFunctions.js"></script>
    </body>
</html>
