<?php

if(!isset($_SERVER['connection'])){
    try {
        $conn = new PDO("sqlsrv:server = tcp:churchdb.database.windows.net,1433; Database = churchmap", "priest", $_ENV['APPSETTINGS_SQL_PASS']);

        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $_SERVER['connection'] = TRUE;
    }
    catch (PDOException $e) {
        print("Error connecting to SQL Server.");
        die();
    }
}