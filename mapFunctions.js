var map;
var lat;
var lng;
var markersArray = [];
var records = {};

$(function(){

var geocoder = new google.maps.Geocoder();
var infowindow = new google.maps.InfoWindow();

google.maps.event.addListener(map, 'idle', getChurch);

$('#plikKML').bind("change", changeName);
});


function grab(event) {
    $("#szerokoscGeograficzna").val(event.latLng.lat());
    lat=$("#szerokoscGeograficzna").val();
    $("#dlugoscGeograficzna").val(event.latLng.lng());
    lng=$("#dlugoscGeograficzna").val();


    document.getElementById("szerokoscByLocation").innerText = event.latLng.lat();
    document.getElementById("dlugoscByLocation").innerText = event.latLng.lng();


}

function searchByLocation(lng, lat, radius){

    var message = {
        latitude : document.getElementById("szerokoscByLocation").textContent,
        longitude : document.getElementById("dlugoscByLocation").textContent,
        radius : radius,
        method : "map.findchurchbylocation"
    }

    console.log(message);

    $.get('api.php', message, function(data) {
        var response = data['result'];
        console.log(response);
        var array = $('#churchtableByLocation');
        array.empty();
        array.append('<tr> \
                <th class="header">NAZWA</th> \
                <th class="header">ADRES</th> \
                <th class="header">DŁUGOŚĆ</th> \
                <th class="header">SZEROKOŚĆ</th> \
                <th class="header">AKCJA</th> \
                </tr>');

        $.each(response, function(key, value) {
            var name = value['name'];
            var url = value['description'];
            var lat = value['latitude'];
            var lng = value['longitude'];
            var id = value['id'];

            var row = "<tr id='" + id + "ByLocation'></tr>";
            array.append(row);
            $('#'+id+"ByLocation").append('<td>'+ name +'</td>');
            $('#'+id+"ByLocation").append('<td>'+ url +'</td>');
            $('#'+id+"ByLocation").append('<td>'+ lng +'</td>');
            $('#'+id+"ByLocation").append('<td>'+ lat +'</td>');
            var showButton = '<button class="pokaz" onclick="hideAndMoveTableByLocation(' + lat + ', ' + lng + ')">POKAŻ</button>';
            $('#'+id+"ByLocation").append('<td>'+ showButton + '</td>');

            var adjust = $('#churchtableByLocation').height() + 100;
            if(adjust < $(window).height()){
                $('#tableContainerByLocation').height($(window).height());
            }
            else {
                $('#tableContainerByLocation').height(adjust);
            }

        })



    }, 'json');
}


function hideAndMoveTableByLocation(latitude, longitude){
    var center = {lat: latitude, lng: longitude};
    map.setCenter(center);
    hideTableByLocation();
}




function showTableSearchByLocation(){

    console.log("test");

    var radius = parseInt($("#radius").val());

    if(radius > 20000){
        alert("Odleglość nie może być większa niż 20 000 km!");
        return;
    }

    radius = radius * 0.0089930749006589;

    searchByLocation(lng, lat, radius);

    $('#tableContainerByLocation').removeAttr('hidden');

    $('#mapa')[0]['attributes']['style']['value'] = "position: fixed; overflow: hidden;";

}
function hideTableByLocation() {
    $('#tableContainerByLocation').attr('hidden', true);
}

















function addToDatabase() {
    $.post('api.php',{
        latitude : $("#szerokoscGeograficzna").val(),
        longitude : $("#dlugoscGeograficzna").val(),
        url : $('#adresKosciola').val(),
        name : $('#nazwaKosciola').val(),
        method : 'map.addchurch'
    });
}

function getChurch() {
    bounds = map.getBounds();

    var message = {
        northBoundary : bounds.getNorthEast().lat(),
        eastBoundary : bounds.getNorthEast().lng(),
        southBoundary : bounds.getSouthWest().lat(),
        westBoundary : bounds.getSouthWest().lng(),
        method : "map.getchurch"
    }

    $.get('api.php', message, function(data) {
        var response = data['result'];

        $.each(response, function(key, value) {
            var marker = createMarker(value);
            markersArray.push(marker);
            google.maps.event.addListener(marker, 'click', function() {
                        if(!this.showing){
                            this.info.open(map, this);
                            this.showing = true;
                        }
                        else{
                            this.info.close();
                            this.showing = false;
                        }
            });
        })
        populateTable(response)

    }, 'json');
}

function createMarker(church) {
    var coords = {lat :church['latitude'], lng: church['longitude']};
    var mark = new google.maps.Marker({
            position: coords,
            showing: false,
            map: map,
            id : church['id'],
            title: church['name'],
            info: new google.maps.InfoWindow({
                content: 'Nazwa: ' + church['name'] +
                '<br/>Adres: '+ church['description']
            })
        });
    return mark;
}

function showTable() {
    $('#tableContainer').removeAttr('hidden');
    adjustTableHeight();
    $('#mapa')[0]['attributes']['style']['value'] = "position: fixed; overflow: hidden;";
}

function adjustTableHeight() {
    var adjust = $('#churchtable').height() + 100;
    if(adjust < $(window).height()){
        $('#tableContainer').height($(window).height());
    }
    else {
        $('#tableContainer').height(adjust);
    }
}

function hideTable() {
    $('#tableContainer').attr('hidden', true);
}

function populateDict(data) {
    records[data['id']] = data;
}

function populateTable(response) {

    var array = $('#churchtable');
    array.empty();
    array.append('<tr> \
                <th class="header">NAZWA</th> \
                <th class="header">ADRES</th> \
                <th class="header">DŁUGOŚĆ</th> \
                <th class="header">SZEROKOŚĆ</th> \
                <th class="header">AKCJA</th> \
                </tr>');

        $.each(response, function(key, value) {
            var name = value['name'];
            var url = value['description'];
            var lat = value['latitude'];
            var lng = value['longitude'];
            var id = value['id'];

            var row = "<tr id='" + id + "'></tr>";
            array.append(row);
            $('#'+id).append('<td>'+ name +'</td>');
            $('#'+id).append('<td>'+ url +'</td>');
            $('#'+id).append('<td>'+ lng +'</td>');
            $('#'+id).append('<td>'+ lat +'</td>');
            var delButton = '<button class="usun" onclick="removeFromDBAndClear(' + id + ')">USUŃ</button>';
            var editButton = '<button class="usun" onclick="editChurch(' + id + ')">EDYTUJ</button>';
            $('#'+id).append('<td>'+ editButton + delButton +'</td>');

        })
}

function removeFromDBAndClear(id) {
    $.post('api.php', {id : id, method : 'map.delchurch'}, function(data) {
            var name = $('#'+id)[0]['cells'][0].innerText;
            $('#' + data).remove();
            $.each(markersArray, function(index, value) {
                if(markersArray[index] != null && markersArray[index]['id'] == data) {
                    markersArray[index].setMap(null);
                    delete markersArray[index];
                    return true;
                }
            });
            adjustTableHeight();
    }, 'json');
}

function editChurch(id) {
    var row = $('#'+id)[0]['cells'];
    var name = row[0].innerText;
    var url = row[1].innerText;
    var lng = row[2].innerText;
    var lat = row[3].innerText;
    var buttons = row[4].innerText;

    row[0].innerHTML = '<input type="text" name="name" id="znazwaKosciola" value="' + name + '" required />';
    row[1].innerHTML = '<input type="text" name="url" id="zadresKosciola" value="' + url + '" required />';
    row[2].innerHTML = '<input type="text" name="longitude" id="zdlugoscGeograficzna" value="' + lng + '" required />';
    row[3].innerHTML = '<input type="text" name="latitude" id="zszerokoscGeograficzna" value="' + lat + '" required />';
    row[4].innerHTML = '<button class="usun" onclick="applyChanges(' + id + ')">ZAKOŃCZ</button>';

}


function applyChanges(id) {
    var name = $('#znazwaKosciola').val();
    var url = $('#zadresKosciola').val();
    var lng = $("#zdlugoscGeograficzna").val();
    var lat = $("#zszerokoscGeograficzna").val();
    var delButton = '<button class="usun" onclick="removeFromDBAndClear(' + id + ')">USUŃ</button>';
    var editButton = '<button class="usun" onclick="editChurch(' + id + ')">EDYTUJ</button>';

    $.post('api.php',{
        id : id,
        latitude : lat,
        longitude : lng,
        url : url,
        name : name,
        method : 'map.editchurch'
    }, function(data) {
        data = data[0];
        var updated = $('#'+data['id'])[0]['cells'];
        updated[0].innerHTML = data['name'];
        updated[1].innerHTML = data['description'];
        updated[2].innerHTML = data['longitude'];
        updated[3].innerHTML = data['latitude'];
        updated[4].innerHTML = editButton + delButton;

        $.each(markersArray, function(index, value) {
                if(markersArray[index] != null && markersArray[index]['id'] == data) {
                    markersArray[index]['info'].setContent(
                        'Nazwa: ' + name +
                        '<br/>Adres: '+ url
                    );
                    markersArray[index].setPosition(
                        {lat: parseFloat(lat), lng: parseFloat(lng)}
                    );
                    return true;
                }
        });

    }, 'json');
}

$("#name").keyup(function(){
    var name = $("#name").val();

    var message = {
        method: 'map.findchurch',
        churchName: name
    }
    if (name.length > 3) {

        $('#records').empty();
        $.get('api.php', message, function(data) {
            $.each(data['result'], function(index, value) {
                console.log(value);
                var item = "<span" +
                " onclick='moveMap(" + value['id'] + ")'>"
                + value['name'] +"</span></br>";
                populateDict(value);
                $('#records').append(item);
            })
        }, 'json');
    }
    else {
        $('#records').empty();
    }

 });

 function moveMap(id) {
     var center = {lat: records[id]['latitude'], lng: records[id]['longitude']};
     map.setCenter(center);
 }

 function uploadKML() {
    if ($('#plikKML') != null) {
        var file = document.querySelector('#plikKML').files[0];
        var xhr = new XMLHttpRequest();
        xhr.open("POST", 'api.php', true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {

                if(xhr.responseText != "OK"){

                    alert("Załadowany plik KML jest niepoprawny!");
                }

                console.log(xhr.responseText);
                document.getElementById('etykieta').firstChild.data = "Plik KML";
            }
        };
        var fd = new FormData();
        fd.append("upload_file", file);
        xhr.send(fd);
        getChurch();
    }
    
}

function changeName() {
    if ($('#plikKML') != null) {
        var filename = document.querySelector('#plikKML').files[0].name;
        var name = document.getElementById('etykieta').firstChild;
        name.data = filename;
    }
}