<?php

require_once 'config.php';
require_once 'apiMethods/user.php';
require_once 'apiMethods/map.php';
require_once 'apiMethods/kml.php';

session_start();
$map = new map($conn);
$kml = new kml($conn, $map);

if(isset($_POST['method'])) {
    $methods = preg_split('/\./', $_POST['method']);
    $supermethod = $methods[0];
    $method = $methods[1];

    if($supermethod == "user") {
        $user = new user($conn);
        if($method == "login") {
            $login = filter_input(INPUT_POST, 'login');
            $pass = filter_input(INPUT_POST, 'password');

            $user->login($login, $pass);
        }
        if($method == "logout"){
            $user->logout();
        }
    }

    if($_SESSION['logged'] != TRUE) {
        echo "Not Authorized";
        exit;
    }

    if($supermethod == "map") {
        $lat = filter_input(INPUT_POST, 'latitude');
        $lng = filter_input(INPUT_POST, 'longitude');
        $url = filter_input(INPUT_POST, 'url');
        $name = filter_input(INPUT_POST, 'name');

        if($method == "addchurch"){
            $map->addChurch($lat, $lng, $url, $name);
            header('Location: adminPanel.php');
            die();
        }
        elseif($method == "editchurch"){
            $id = filter_input(INPUT_POST, 'id');
            echo json_encode($map->editChurch($id, $lat, $lng, $url, $name));
        }
        elseif($method == "delchurch"){

            $id = filter_input(INPUT_POST, 'id');
            $map->deleteChurch($id);
        }
    }

}
if( isset($_SESSION['logged']) && $_SESSION['logged'] == TRUE && isset($_FILES['upload_file'])) {
    try {

        $file = $_FILES['upload_file']['tmp_name'];
        if(!$kml->validate($file)){
            echo "false;" + $kml->getErrorMessage();
            return;
        }
        $kml->importKML($file);
        echo "OK";
    }
    catch (Exception $e) {
        echo $e->getmessage(). " Failed";
    }

}

if(isset($_GET['method'])) {
    $methods = preg_split('/\./', $_GET['method']);
    $supermethod = $methods[0];
    $method = $methods[1];
    if($supermethod == "map") {
        if($method == "getchurch"){
            $north = filter_input(INPUT_GET, 'northBoundary');
            $south = filter_input(INPUT_GET, 'southBoundary');
            $west = filter_input(INPUT_GET, 'westBoundary');
            $east = filter_input(INPUT_GET, 'eastBoundary');

            echo json_encode($map->getChurch($north, $south, $west, $east));
        }
        elseif($method == "getallchurch"){
            echo json_encode($map->getAllChurches());
        }
        elseif($method == "findchurch"){
            $name = filter_input(INPUT_GET, 'churchName');

            echo json_encode($map->findChurch($name));
        }elseif($method == "findchurchbylocation"){
            $lat = filter_input(INPUT_GET, 'latitude');
            $lng = filter_input(INPUT_GET, 'longitude');
            $radius = filter_input(INPUT_GET, 'radius');

            echo json_encode($map->findChurchByLocation($lng, $lat, $radius));
        }
    }
    elseif($supermethod == "kml") {
        if($method == "exportkml"){
            $kml->exportkml();
        }
    }
}